import { MulterModuleOptions } from '@nestjs/platform-express';
import { diskStorage } from 'multer';

import { tmpdir } from 'os';
import { extname } from 'path';
import { randomUUID } from 'crypto';

const storage = diskStorage({
  destination: tmpdir(),
  filename: (req, file, cb) => {
    cb(null, randomUUID() + extname(file.originalname));
  },
});

export const options: MulterModuleOptions = {
  storage,
};
