import { Module } from '@nestjs/common';
import { BullModule } from '@nestjs/bull';

import { TransformationsController } from './transformations.controller';
import { TransformationsConsumer } from './transformations.consumer';
import { TransformationsProcessor } from './transformations.processor';
import { QUEUES } from './constants';

@Module({
  imports: [
    BullModule.registerQueue({
      name: QUEUES.TRANSFORMATIONS,
      limiter: {
        max: 1,
        duration: 5000,
      },
    }),
  ],
  controllers: [TransformationsController],
  providers: [TransformationsConsumer, TransformationsProcessor],
})
export class TransformationsModule {}
