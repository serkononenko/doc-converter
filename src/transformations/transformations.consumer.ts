import { Injectable } from '@nestjs/common';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';

import { createReadStream } from 'fs';
import { unlink } from 'fs/promises';

import { EXTENSIONS, QUEUES, JOBS } from './constants';

@Injectable()
export class TransformationsConsumer {
  constructor(@InjectQueue(QUEUES.TRANSFORMATIONS) private readonly convertQueue: Queue) {}

  async transform(filePath: string, extension: EXTENSIONS) {
    const job = await this.convertQueue.add(JOBS.TRANSFORM, {
      filePath,
      extension,
    });

    const path = await job.finished();

    const stream = createReadStream(path);

    stream.on('close', () => {
      unlink(filePath);
      unlink(path);
    });

    return stream;
  }
}
