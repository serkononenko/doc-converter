export enum EXTENSIONS {
  PPTX = 'pptx',
  DOCX = 'docx',
  PDF = 'pdf',
}

export const QUEUES = {
  TRANSFORMATIONS: 'transformations',
};

export const JOBS = {
  TRANSFORM: 'transform',
};
