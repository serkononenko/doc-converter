import { Controller, Post, Param, Res, UseInterceptors, UploadedFile } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiParam } from '@nestjs/swagger';
import { Response } from 'express';

import { TransformationsConsumer } from './transformations.consumer';
import { getFileName } from '../utils';
import { options } from './multer';
import { EXTENSIONS } from './constants';

@Controller('transformations')
export class TransformationsController {
  constructor(private transformationsConsumer: TransformationsConsumer) {}

  @Post(':extension')
  @ApiParam({ name: 'extension', enum: EXTENSIONS })
  @UseInterceptors(FileInterceptor('file', options))
  async transformToExtension(
    @Param('extension') extension: EXTENSIONS,
    @UploadedFile() { path }: Express.Multer.File,
    @Res() res: Response,
  ) {
    const stream = await this.transformationsConsumer.transform(path, extension);

    res.set({
      'Content-Disposition': `attachment; filename="${getFileName(extension)}"`,
    });

    stream.pipe(res);
  }
}
