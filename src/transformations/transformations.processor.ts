import { ServiceUnavailableException } from '@nestjs/common';
import { Process, Processor } from '@nestjs/bull';
import { Job } from 'bull';

import { promisify } from 'util';
import { execFile } from 'child_process';
import { extname } from 'path';

import { getConverter, getArgs } from '../utils';
import { QUEUES, JOBS } from './constants';

const executeFile = promisify(execFile);

@Processor(QUEUES.TRANSFORMATIONS)
export class TransformationsProcessor {
  private readonly converter: string = getConverter();

  @Process(JOBS.TRANSFORM)
  async handleTransform(job: Job) {
    const { filePath, extension } = job.data;

    const args = getArgs(filePath, extension);

    const { stderr, stdout } = await executeFile(this.converter, args);

    if (stdout) {
      return filePath.replace(extname(filePath), '.' + extension);
    } else {
      throw new ServiceUnavailableException(stderr);
    }
  }
}
