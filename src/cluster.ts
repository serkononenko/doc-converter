import * as c from 'cluster';
import { cpus } from 'os';

import type { Cluster } from 'cluster';

const numCPUs = cpus().length;
const noop = () => undefined;
const cluster = c as unknown as Cluster;

export default (callback: () => void = noop) => {
  if (cluster.isPrimary) {
    console.log(`Master ${process.pid} is running`);
    // Fork workers.
    for (let i = 0; i < numCPUs; i++) {
      cluster.fork();
    }
    cluster.on('exit', (worker) => {
      console.log(`worker ${worker.process.pid} died`);
    });
  } else {
    callback();
    console.log(`Worker ${process.pid} started`);
  }
};
