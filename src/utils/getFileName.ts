const PREFIX = 'PitchBook_Market_Map';

export const getFileName = (extension: string) => `${PREFIX}.${extension}`;
