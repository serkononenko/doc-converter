import { tmpdir } from 'os';
import { extname } from 'path';

import { EXTENSIONS } from '../transformations/constants';

const IMPORT_FILTERS = {
  [EXTENSIONS.PDF]: {
    [EXTENSIONS.PPTX]: '--infilter=impress_pdf_import',
    [EXTENSIONS.DOCX]: '--infilter=writer_pdf_import',
  },
};

const getImportFilter = (path: string, outputExtension: string): string => {
  const inputExtension = extname(path).slice(1);

  return IMPORT_FILTERS[inputExtension]?.[outputExtension] || '';
};

export const getArgs = (path: string, extension: EXTENSIONS): Array<string> => [
  getImportFilter(path, extension),
  '--headless',
  '--convert-to',
  extension,
  path,
  '--outdir',
  tmpdir(),
];
