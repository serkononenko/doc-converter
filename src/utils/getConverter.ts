import { join } from 'path';
import { platform, env } from 'process';

export const getConverter = () => {
  switch (platform) {
    case 'win32':
      return join(env.ProgramFiles, 'LibreOffice', 'program', 'soffice.exe');
    case 'linux':
      return join('/usr', 'bin', 'soffice');
    default:
      throw new Error("Can't find libreoffice converter");
  }
};
