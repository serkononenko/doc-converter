FROM node:14-alpine

WORKDIR /app

COPY ["package.json", "package-lock.json*", "./"]

RUN apk add --update libreoffice && npm install && npm i -g @nestjs/cli

COPY . .

EXPOSE 3000

CMD npm run deploy